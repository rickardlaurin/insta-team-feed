 module.exports = (function () {
  'use strict';

  function Saved () {

    this.savedUsersResponse = {};
    var self = this;

    Date.prototype.addMinutes = function (m) {
      m = typeof m === 'number' ? m : 0;
      this.setMinutes(this.getMinutes() + m);
      return this;
    };

    setInterval(function () {
      var now, copy, value;
      now = new Date();
      copy = self.savedUsersResponse;
      for (value in copy) {
        if (typeof copy[value] !== 'object') {
          continue;
        }
        if (!copy[value].data ||
            !copy[value].date ||
            typeof copy[value].count !== 'number') {
          delete self.savedUsersResponse[value];
          continue;
        }
        if (now - copy[value].date > 600000) {
          delete self.savedUsersResponse[value];
          continue;
        }
      }
    }, 1500);

    this.get = function (opt) {
      var self = this;
      if (typeof opt !== 'object') {
        return;
      }
      var previous = this.savedUsersResponse[opt.id];
      if (typeof previous !== 'object' || !previous.data) {
        return false;
      }
      if (previous.date > new Date().addMinutes(10)) {
        return false;
      }
      if (previous.count < opt.count) {
        return false;
      }
      var data = previous.data.slice(0, opt.count);
      var error = previous.error || null;
      return {
        "data": data,
        "status": 200,
        "error": error
      };
    };

    this.set = function (obj) {
      if (typeof obj !== 'object' ||
          !obj.id) {
        return;
      }

      var data = obj.data;
      var error = obj.error || null;
      var count = parseInt(obj.count, 10) || 10;

      data = data.map(function (inner) {
        inner.title = inner.title instanceof Array ? inner.title[0] : inner.title;
        inner.link = inner.link instanceof Array ? inner.link[0] : inner.link;
        inner.description = inner.description instanceof Array ? inner.description[0] : inner.description;
        inner.pubDate = inner.pubDate instanceof Array ? inner.pubDate[0] : inner.pubDate;
        inner.creator = inner['dc:creator'] instanceof Array ? inner['dc:creator'][0] : inner['dc:creator'];
        try {
          inner.pubDate = new Date(inner.pubDate);
        } catch (parseError) {
          console.error(parseError);
        }
        return {
          "title": inner.title || null,
          "link": inner.link || null,
          "content": inner.description || null,
          "pubDate": inner.pubDate || null,
          "creator": inner.creator || null
        };
      });
      this.savedUsersResponse[obj.id] = {
        "date": new Date(),
        "count": count,
        "data": data,
        "error": error
      };
      console.log('new save: %s', obj.id);
      return data.slice(0).splice(0, count);
    };
  }

  return new Saved();
})();